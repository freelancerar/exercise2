package ar.comviva.exercisetwo.db;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.Iterator;
import java.util.Properties;
import java.util.UUID;
import java.util.stream.Stream;

import ar.comviva.exercisetwo.exceptions.PropertiesFileException;


/**
 * @author lester
 * Executes the logic for saving in the database
 */
public class DBProcessor {
	//queries
	private static final String EXIST_TABLE_QUERY = "SELECT * " + 
												  "FROM information_schema.tables " +
												  "WHERE table_schema = ? AND table_name = 'file_processing' " +
												  "LIMIT 1";
	
	private static final String CREATE_TABLE_QUERY = "CREATE TABLE file_processing (" +
												  	"file_processing_id  varchar(32) NOT NULL, " +
												  	"file_name  varchar(255) NOT NULL, " +
												  	"file_value  float NOT NULL, " +
												  	"process_date  timestamp NOT NULL" +
												   ")";
	
	private static final String ADD_PRIMARY_KEY_QUERY = "ALTER TABLE file_processing " + 
		    									     "ADD CONSTRAINT PK_FILEPROCESSINGID PRIMARY KEY (file_processing_id)";
	
	private static final String INSERT_QUERY = "INSERT INTO file_processing (file_processing_id, file_name, file_value, process_date) VALUES (?,?,?,?)";
	
	
	//methods
	/**
	 * Inserts in the database the new record with the information of a file processing
	 * @param fileName
	 * @param value
	 * @return affected rows number
	 * @throws SQLException 
	 * @throws IOException 
	 * @throws PropertiesFileException 
	 */
	public static int insertInDBNewRecordOfFileProcessing(String fileName, float value) throws SQLException, IOException, PropertiesFileException{
		Connection conn = null;
		SQLException ex = null;
		int affectedRows = 0;
		
		try {
			conn = getConnection();
			
			checkIfTableExists(conn);
			
			affectedRows = insertRow(conn, fileName, value);
		} catch (SQLException e) {
			ex = e;
		}finally {
			if(conn != null)
				conn.close();
		}
		
		if(ex != null)
			throw ex;
		
		return affectedRows;
	}
	
	/**
	 * Inserts a new row in the database
	 * @param conn
	 * @param fileName
	 * @param value
	 * @throws SQLException
	 */
	private static int insertRow(Connection conn, String fileName, float value) throws SQLException {
		PreparedStatement preparedStatement = conn.prepareStatement(INSERT_QUERY);

        preparedStatement.setString(1, UUID.randomUUID().toString().replace("-", "").toUpperCase());
        preparedStatement.setString(2, fileName);
        preparedStatement.setBigDecimal(3, new BigDecimal(value));
        preparedStatement.setTimestamp(4, Timestamp.valueOf(LocalDateTime.now()));
        

        int affectedRows = preparedStatement.executeUpdate();
        
        return affectedRows;
	}
	
	
	/**
	 * @return a set with the keys and its values
	 * @throws FileNotFoundException 
	 * @throws PropertiesFileException 
	 * @throws IOException 
	 */
	private static Properties getProperties() throws FileNotFoundException, PropertiesFileException {	
		String URL = "src/main/resources/config/database.properties";
		
		File file = new File(URL);
		
		if(!file.exists()){
			URL = "../database.properties";
		}
		
		InputStream input = new FileInputStream(URL);

        Properties prop = new Properties();

        try {
			prop.load(input);
		} catch (IOException e) {
			throw new PropertiesFileException();
		}            

		return prop;
	}
	
	/**
	 * @return JDBC connection
	 * @throws SQLException
	 * @throws PropertiesFileException 
	 * @throws FileNotFoundException 
	 */
	private static Connection getConnection() throws SQLException, FileNotFoundException, PropertiesFileException{
		Properties prop = getProperties();
		
		String dbName = prop.getProperty("db.database");
		String host = prop.getProperty("db.host");
		String user = prop.getProperty("db.username");
		String pass = prop.getProperty("db.pass");
		String port = prop.getProperty("db.port");
		
		Connection conn = DriverManager.getConnection("jdbc:mysql://" + host + ":" + port + "/" + dbName + "", user, pass);
		
		return conn;
	}
	
	/**
	 * Checks if the core table of the exercise exists in the database
	 * @param conn JDBC Connection
	 * @throws SQLException 
	 */
	private static void checkIfTableExists(Connection conn) throws SQLException {
		//checking stuff
		String dbName = "exercise2";
		
		PreparedStatement ps = conn.prepareStatement(EXIST_TABLE_QUERY);
	    ps.setString(1, dbName);
	    
	    ResultSet rs = ps.executeQuery();
	    
	    boolean exists = rs.first();
	    
	    if(!exists) {//it's necessary create the table	    		    
	    	ps = conn.prepareStatement(CREATE_TABLE_QUERY);
	    	ps.execute();
	    	
	    	ps = conn.prepareStatement(ADD_PRIMARY_KEY_QUERY);
	    	ps.execute();	    		    
	    }
	}
	
	
	
}
