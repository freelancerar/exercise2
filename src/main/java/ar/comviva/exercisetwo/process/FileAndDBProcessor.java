package ar.comviva.exercisetwo.process;

import java.io.IOException;
import java.sql.SQLException;

import ar.comviva.exercisetwo.db.DBProcessor;
import ar.comviva.exercisetwo.dto.ResultsDTO;
import ar.comviva.exercisetwo.exceptions.FileDoesNotExistException;
import ar.comviva.exercisetwo.exceptions.FileIsNotTXTException;
import ar.comviva.exercisetwo.exceptions.PropertiesFileException;
import ar.comviva.exercisetwo.file.FileProcessor;

public class FileAndDBProcessor {
	public static String processFile(String filePath) throws FileDoesNotExistException, FileIsNotTXTException, IOException, SQLException, PropertiesFileException {
		ResultsDTO results = FileProcessor.extractAndProcessNumbersFromFile(filePath);				
		
		DBProcessor.insertInDBNewRecordOfFileProcessing(filePath, results.getSum());
		
		return results.getSummaryExpression();
	}
}
