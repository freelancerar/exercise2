package ar.comviva.exercisetwo;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Scanner;

import ar.comviva.exercisetwo.exceptions.FileDoesNotExistException;
import ar.comviva.exercisetwo.exceptions.FileIsNotTXTException;
import ar.comviva.exercisetwo.exceptions.PropertiesFileException;
import ar.comviva.exercisetwo.process.FileAndDBProcessor;

/**
 * @author lester
 * Console application
 */
public class Main {

	
	public static void main(String[] args) {
		
		
		Scanner keyboard = new Scanner(System.in);
		
		boolean exit = false;		
		
		//messages
		String welcomeMSG = "Welcome to Problem #2";
		String filePathMSG = "Please enter the path of the file you want to process";
		String blankFilePathMSG = "File path cannot be empty.";
		String fileDoesNotExistMSG = "File does not exist";
		String fileIsNotTXTMSG = "The file is not a txt file";
		String unexpectedErrorMSG = "Sorry, there was an unexpected error while processing your file";
		String resultMSG = "The result of processing the file is shown below:";
		String exitMSG = "If you want to exit the application press 'Q', otherwise press any other key to continue";		
		String thanksMSG = "Thanks for using this application";
		String sqlErrorMSG = "An error ocurred while accessing the database has ocurred, please review its detail: ";
		String propertiesFileErrorMSG = "An error ocurred while trying to access the properties file. Please review carefully the"
				                         + " README.md file with the explanations on how configure properly this file";
		
		
		System.out.println(welcomeMSG);
		
		//main loop
		do {									
			System.out.println(filePathMSG);
			
			boolean pathFileIsNotBlank = false;
			
			//filePath loop
			String filePath = "";
			do {
				filePath = keyboard.nextLine();
				
				if(filePath == null || filePath.equals(""))
					System.out.println(blankFilePathMSG);
				else
					pathFileIsNotBlank = true;
			}while(!pathFileIsNotBlank);
			
			//file processing and results showing
			try {
				String result = FileAndDBProcessor.processFile(filePath);
				
				System.out.println(resultMSG);
				System.out.println(result);
				
			} catch (FileDoesNotExistException e) {
				System.out.println(fileDoesNotExistMSG);
			} catch (FileIsNotTXTException e) {
				System.out.println(fileIsNotTXTMSG);
			} catch (IOException e) {
				System.out.println(unexpectedErrorMSG);
			} catch (SQLException e) {
				System.out.println(sqlErrorMSG);
				System.out.println(e.getMessage());
			} catch (PropertiesFileException e) {
				System.out.println(propertiesFileErrorMSG);
			}
			
			//exit verification
			System.out.println(exitMSG);
			
			char exitOption = keyboard.nextLine().charAt(0);
			
			if(exitOption == 'Q' || exitOption == 'q')
				exit = true;
			
		}while(!exit);

		//exit stuff
		System.out.println(thanksMSG);
		
		keyboard.close();
	}

}
