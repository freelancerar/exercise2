package ar.comviva.exercisetwo.dto;

/**
 * @author lester
 * Wraps the results of the file processing
 */
public class ResultsDTO {
	private String summaryExpression;
	private float sum;
	
	
	/**
	 * @param summaryExpression
	 * @param sum
	 */
	public ResultsDTO(String summaryExpression, float sum) {
		super();
		this.summaryExpression = summaryExpression;
		this.sum = sum;
	}


	public String getSummaryExpression() {
		return summaryExpression;
	}


	public void setSummaryExpression(String summaryExpression) {
		this.summaryExpression = summaryExpression;
	}


	public float getSum() {
		return sum;
	}


	public void setSum(float sum) {
		this.sum = sum;
	}
	
	
	
}
