# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

Este es el repositorio GIT correspondiente al problema 2

El proyecto fue construido utilizando un proyecto Maven básico con una aplicación de Consola y el IDE Spring Tool Suite 4

El proyecto fue diseñado para funcionar utilizando una base de datos MySQL

### How do I get set up? ###

* Configurar el archivo \src\main\resources\config\database.properties con los datos de conexión a la base de datos MySQL
  NOTA: una vez generado el jar, también se debe poner una copia del archivo database.properties en un directorio más arriba de donde esté ubicado el jar.
  Estoy consciente de que es una mala práctica pero tuve problemas accediendo al archivo de propiedades desde dentro del .jar y dado que el plazo de entrega
  fue acortado de 7 días a 2 días debo enviar ya las soluciones.
  
* Ejecutar la tarea de Maven "install" para que se genere el jar de la aplicación
* Ejecutar el jar generado en "target/exercisetwo-0.0.1-SNAPSHOT-jar-with-dependencies.jar" (basados en el primer punto se debería tener un archivo 
  database.properties en la raíz del proyecto exercisetwo)

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Lester Hernandez (lestercujae@gmail.com)